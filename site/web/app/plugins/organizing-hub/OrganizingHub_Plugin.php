<?php


include_once('OrganizingHub_LifeCycle.php');

class OrganizingHub_Plugin extends OrganizingHub_LifeCycle {

    /**
     * See: http://plugin.michael-simpson.com/?page_id=31
     * @return array of option meta data.
     */
    public function  getOptionMetaData() {
        //  http://plugin.michael-simpson.com/?page_id=31
        return array(
            //'_version' => array('Installed Version'), // Leave this one commented-out. Uncomment to test upgrades.
            'ATextInput' => array(__('Enter in some text', 'organizing-hub')),
            'AmAwesome' => array(__('I like this awesome plugin', 'organizing-hub'), 'false', 'true'),
            'CanDoSomething' => array(__('Which user role can do something', 'organizing-hub'),
                                        'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber', 'Anyone')
        );
    }

//    protected function getOptionValueI18nString($optionValue) {
//        $i18nValue = parent::getOptionValueI18nString($optionValue);
//        return $i18nValue;
//    }

    protected function initOptions() {
        $options = $this->getOptionMetaData();
        if (!empty($options)) {
            foreach ($options as $key => $arr) {
                if (is_array($arr) && count($arr > 1)) {
                    $this->addOption($key, $arr[1]);
                }
            }
        }
    }

    public function  getPluginDisplayName() {
        return 'Organizing Hub';
    }

    protected function getMainPluginFileName() {
        return 'organizing-hub.php';
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Called by install() to create any database tables if needed.
     * Best Practice:
     * (1) Prefix all table names with $wpdb->prefix
     * (2) make table names lower case only
     * @return void
     */
    protected function installDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("CREATE TABLE IF NOT EXISTS `$tableName` (
        //            `id` INTEGER NOT NULL");
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Drop plugin-created tables on uninstall.
     * @return void
     */
    protected function unInstallDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("DROP TABLE IF EXISTS `$tableName`");
    }


    /**
     * Perform actions when upgrading from version X to version Y
     * See: http://plugin.michael-simpson.com/?page_id=35
     * @return void
     */
    public function  upgrade() {
    }

    public function  addActionsAndFilters() {

        // Add options administration page
        // http://plugin.michael-simpson.com/?page_id=47
        add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));
        add_action('init', array(&$this, 'registerCustomPostTypes'));
        add_action( 'add_meta_boxes', array(&$this, 'petition_box'));
        add_action('save_post', array(&$this, 'save_petition_hero_config'), 1, 2);
        add_filter( '_wp_post_revision_field_my_meta', array(&$this, 'display_revisions_field_petition_hero_config'), 10, 2 );
        add_filter( '_wp_post_revision_fields', array(&$this, 'get_revisions_field_petition_hero_config') );
        add_action( 'wp_restore_post_revision', array(&$this, 'restore_revisions_petition_hero_config'), 10, 2 );
		add_action( 'save_post', array(&$this, 'save_revisions_petition_hero_config') );
	    add_action( 'admin_enqueue_scripts', array(&$this, 'add_petition_scripts'), 10, 1 );

        // Example adding a script & style just for the options administration page
        // http://plugin.michael-simpson.com/?page_id=47
        //        if (strpos($_SERVER['REQUEST_URI'], $this->getSettingsSlug()) !== false) {
        //            wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));
        //            wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
        //        }


        // Add Actions & Filters
        // http://plugin.michael-simpson.com/?page_id=37


        // Adding scripts & styles to all pages
        // Examples:
        //        wp_enqueue_script('jquery');
        //        wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
        //        wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));


        // Register short codes
        // http://plugin.michael-simpson.com/?page_id=39


        // Register AJAX hooks
        // http://plugin.michael-simpson.com/?page_id=41

    }

    public function  registerCustomPostTypes() {

        /**
         * Post Type: petitions.
         */

        $labels = array(
            "name" => __( "Petitions", 'organizing-hub' ),
            "singular_name" => __( "Petition", 'organizing-hub' ),
            "menu_name" => __( "Petitions", 'organizing-hub' ),
            "all_items" => __( "All Petitions", 'organizing-hub' ),
        );

        $args = array(
            "label" => __( "Petitions", 'organizing-hub' ),
            "labels" => $labels,
            "description" => "A post of this type will display it\'s featured image as a CSS background image for a hero area in the site.",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "show_in_rest" => false,
            "rest_base" => "",
            "has_archive" => false,
            "show_in_menu" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( "slug" => "petition", "with_front" => true ),
            "query_var" => true,
            "supports" => array( "title", "editor", "thumbnail", "custom-fields", "revisions" ),
            "taxonomies" => array( "category", "post_tag" ),
        );

        register_post_type( "petition", $args );

        /**
         * Post Type: news.
         */

        $labels = array(
            "name" => __( "News", 'organizing-hub' ),
            "singular_name" => __( "News Article", 'organizing-hub' ),
            "menu_name" => __( "News", 'organizing-hub' ),
            "all_items" => __( "All News Articles", 'organizing-hub' ),
        );

        $args = array(
            "label" => __( "News", 'organizing-hub' ),
            "labels" => $labels,
            "description" => "These article excerpts will be displayed along with Twitter feed nodes.",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "show_in_rest" => false,
            "rest_base" => "",
            "has_archive" => false,
            "show_in_menu" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( "slug" => "news", "with_front" => true ),
            "query_var" => true,
            "supports" => array( "title", "editor", "thumbnail", "custom-fields", "revisions" ),
            "taxonomies" => array( "category", "post_tag" ),
        );

        register_post_type( "news", $args );
    }

    /**
	 * Create the metabox default values
	 */
	protected function petition_metabox_defaults() {
		return array(
			'overlay' => 'off',
			'min_height' => '',
			'overlay_color' => '',
			'additional_background_css' => 'background-repeat:no-repeat;background-size:cover;background-position:center center',
		);
	}


	/**
	 * Create a metabox
	 */
	public function  petition_box() {

		add_meta_box( 'petition_hero_config', 'Petition Page Display', array(&$this, 'petition_hero_config'), 'petition', 'normal', 'default' );

	}

	/**
	 * Add fields to the metabox
	 */
	public function  petition_hero_config() {

		global $post;

		// Get petition hero content
		$saved = get_post_meta( $post->ID, 'petition_hero_config', true );
		$defaults = $this->petition_metabox_defaults();
		$hero = wp_parse_args( $saved, $defaults );

		?>

			<p>Use this section to refine the Banner Display.</p>

			<p>To add a background image to your hero banner, set a <em>Featured Image</em>.</p>

			<fieldset>
				<input type="checkbox" id="petition_overlay" name="petition_overlay" value="on" <?php checked( 'on',  $hero['overlay'] ); ?>>
				<label for="petition_overlay"><?php _e( 'Add a semi-transparent overlay to the background image to make the text easier to read', 'nydems' ); ?></label>
			</fieldset>

			<h3>Minimum Height</h3>

			<fieldset>
				<label for="petition_min_height"><?php printf( __( '[Optional] Make sure the hero never gets too small by providing a minimum height. Example: %s', 'nydems' ), '<code>300px</code>' ); ?></label>
				<input type="text" class="large-text" name="petition_min_height" id="petition_min_height" value="<?php echo stripslashes( $hero['min_height'] ); ?>">
			</fieldset>

			<h3>Overlay Styling</h3>
			Find RGBA codes at: <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Colors/Color_picker_tool">RGB color picker</a>

			<fieldset>
				<label for="petition_overlay_color"><?php printf( __( '[Optional] Change overlay color and transparency from default. Example: %s', 'nydems' ), '<code>rgba(0, 0, 0, 0.5)</code>' ); ?></label>
				<input type="text" class="large-text" name="petition_overlay_color" id="petition_overlay_color" value="<?php echo stripslashes( $hero['overlay_color'] ); ?>">
			</fieldset>

			<h3>Additional Background CSS Styling (Advanced)</h3>

			<fieldset>
				<input type="text" placeholder="background-position: bottom center; background-size: 1500px;" class="large-text" name="petition_additional_background_css" id="petition_additional_background_css" value="<?php echo stripslashes( $hero['additional_background_css'] ); ?>">
				<label for="petition_additional_background_css"><?php _e( 'background-color, background-image, background-repeat, background-attachment, background-position', 'nydems' ); ?></label>
			</fieldset>

		<?php

		// Security field
		wp_nonce_field( 'org-hub-page-hero-nonce', 'org-hub-page-hero-process' );

	}

		/**
	 * Save field data
	 * @param  number $post_id The post ID
	 * @param  Array $post     The post data
	 */
	public function save_petition_hero_config( $post_id, $post ) {

		// Verify data came from edit screen
		if ( !isset( $_POST['org-hub-page-hero-process'] ) || !wp_verify_nonce( $_POST['org-hub-page-hero-process'], 'org-hub-page-hero-nonce' ) ) {
			return $post->ID;
		}

		// Verify user has permission to edit post
		if ( !current_user_can( 'edit_post', $post->ID )) {
			return $post->ID;
		}

		// Get hero data
		$hero = array();

		if ( isset( $_POST['petition_overlay'] ) ) {
			$hero['overlay'] = wp_filter_nohtml_kses( $_POST['petition_overlay'] );
		}

		if ( isset( $_POST['petition_overlay_color'] ) ) {
			$hero['overlay_color'] = wp_filter_nohtml_kses( $_POST['petition_overlay_color'] );
		}

		if ( isset( $_POST['petition_min_height'] ) ) {
			$hero['min_height'] = wp_filter_nohtml_kses( $_POST['petition_min_height'] );
		}

		if ( isset( $_POST['petition_additional_background_css'] ) ) {
			$hero['additional_background_css'] = wp_filter_nohtml_kses( $_POST['petition_additional_background_css'] );
		}

		// Update hero settings
		update_post_meta( $post->ID, 'petition_hero_config', $hero );

	}



	// Save the data with revisions
	public function save_revisions_petition_hero_config( $post_id ) {

		// Check if it's a revision
		$parent_id = wp_is_post_revision( $post_id );

		// If is revision
		if ( $parent_id ) {

			// Get the data
			$parent = get_post( $parent_id );
			$hero = get_post_meta( $parent->ID, 'petition_hero_config', true );

			// If data exists, add to revision
			if ( !empty( $hero ) && is_array( $hero ) ) {

				if ( array_key_exists( 'overlay', $hero ) ) {
					add_metadata( 'petition', $post_id, 'petition_overlay', $hero['overlay'] );
				}

				if ( array_key_exists( 'overlay_color', $hero ) ) {
					add_metadata( 'petition', $post_id, 'petition_overlay_color', $hero['overlay_color'] );
				}

				if ( array_key_exists( 'min_height', $hero ) ) {
					add_metadata( 'petition', $post_id, 'petition_min_height', $hero['min_height'] );
				}

				if ( array_key_exists( 'additional_background_css', $hero ) ) {
					add_metadata( 'petition', $post_id, 'petition_additional_background_css', $hero['additional_background_css'] );
				}
			}

		}

	}



	// Restore the data with revisions
	public function restore_revisions_petition_hero_config( $post_id, $revision_id ) {

		// Variables
		$post = get_post( $post_id );
		$revision = get_post( $revision_id );
		$hero = get_post_meta( $post_id, 'petition_hero_config', true );
		$hero_overlay = get_metadata( 'post', $revision->ID, 'petition_overlay', true );
		$hero_min_height = get_metadata( 'post', $revision->ID, 'petition_min_height', true );
		$hero_overlay_color = get_metadata( 'post', $revision->ID, 'petition_overlay_color', true );
		$hero_additional_background_css = get_metadata( 'post', $revision->ID, 'petition_additional_background_css', true );

		// Update content
		if ( !empty( $hero_overlay ) ) {
			$hero['overlay'] = $hero_overlay;
		}
		if ( !empty( $hero_overlay_color ) ) {
			$hero['overlay_color'] = $hero_overlay_color;
		}
		if ( !empty( $hero_min_height ) ) {
			$hero['min_height'] = $hero_min_height;
		}
		if ( !empty( $hero_additional_background_css ) ) {
			$hero['additional_background_css'] = $hero_additional_background_css;
		}

		update_post_meta( $post_id, 'petition_hero_config', $hero );

	}



	// Get the data to display the revisions page
	public function get_revisions_field_petition_hero_config( $fields ) {
		$fields['petition_overlay'] = 'Petition Background Overlay';
		$fields['petition_min_height'] = 'Petition Minimum Height';
		$fields['petition_overlay_color'] = 'Petition Overlay Styling';
		$fields['petition_additional_background_css'] = 'Petition Additional Background CSS';
		return $fields;
	}



	// Display the data on the revisions page
	public function display_revisions_field_petition_hero_config( $value, $field ) {
		global $revision;
		return get_metadata( 'petition_hero_config', $revision->ID, $field, true );
	}



		// Load required scripts and styles
	public function add_petition_scripts( $hook ) {

        // For now do nothing
        /*
		global $typenow;
		if ( in_array( $typenow, array( 'petition' ) ) ) {
			wp_enqueue_media();
			// Registers and enqueues the required javascript.
			wp_register_script( 'meta-box-image', get_plugin_directory_uri() . '/js/some.js', array( 'jquery' ) );
			wp_localize_script( 'meta-box-image', 'meta_image',
				array(
					'title' => __( 'Choose or Upload an Image', 'nydems' ),
					'button' => __( 'Use this image', 'nydems' ),
				)
			);
			wp_enqueue_script( 'meta-box-image' );
		}*/
	}

		// Get hero content
	public function get_hero_content( $post_id = null ) {

		// Get the post ID
		global $post;

		$post_id = $post_id ? $post_id : $post->ID;

		// Get hero
		$hero = get_post_meta( $post_id, 'petition_hero_config', true );

		$is_hero = is_array( $hero );

		// Get post thumbnail
		$is_thumbnail = has_post_thumbnail( $post_id );
		$thumbnail_id = $is_thumbnail ? get_post_thumbnail_id() : null;

		return array(
			'overlay'              => $is_hero && array_key_exists( 'overlay', $hero ) ? $hero['overlay'] : null,
			'img'                  => $is_thumbnail ? $thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full') : null,
			'min_height'           => $is_hero && array_key_exists( 'min_height', $hero ) ? $hero['min_height'] : null,
			'overlay_color'           => $is_hero && array_key_exists( 'overlay_color', $hero ) ? $hero['overlay_color'] : null,
			'additional_background_css' => $is_hero && array_key_exists( 'additional_background_css', $hero ) ? $hero['additional_background_css'] : null,
		);

	}



	// Determine if a post has a hero
	protected function has_hero( $post_id = null ) {

		// Return false on blog posts and 404 pages
		if ( is_home() || is_single() || is_404() ) return false;

		// Get the post ID
		global $post;

		if ( $post_id = null ) return false;

		// Get hero content
		$hero = $this->get_hero_content( $post_id);

		return ( empty( $hero['img'] ) ) ? false : true;

	}


	// Generate hero markup
	public function display_hero( $post_id = null ) {

		// Get the post ID
		global $post;
        $args = array( 'numberposts' => 1, 'post_type' => 'petition');
        $post = get_posts( $args )[0];
		$post_id = $post_id ? $post_id : $post->ID;
		// Get hero
		$hero = $this->get_hero_content( $post_id );
        $color = $height = $backgroundCSS = $gradient = '';
		preg_match("/^(rgba?)\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3}),\s*(\d*(?:\.\d+)?)\)$/", $hero['overlay_color'], $overlay_color);

		if ( empty( $hero['overlay_color'] ) || count($overlay_color) == 0) {
			$value_type = 'rgba';
			$overlay = '255, 0, 0';
			$transparency = '0.2';
		} else {
			$value_type = $overlay_color[1];
			$overlay = $overlay_color[2] . ', ' . $overlay_color[3] . ', ' . $overlay_color[4];
			$transparency = $overlay_color[5];
		}

        if ($hero['overlay'] == 'on') {
            $gradient = 'linear-gradient( '.$value_type.'('.$overlay.', '.$transparency.'), '.$value_type.'('.$overlay.', '.$transparency.' )), ';
            $overlay_color = empty($hero['overlay_color']) ? '' : $hero['overlay_color'];
            $height = empty($hero['min_height']) ? "min-height: ".$hero['img'][2].";" : "min-height: ".$hero['min_height'].";";
            $backgroundCSS = empty($hero['additional_background_css']) ? 'background-repeat:no-repeat;background-size:cover;background-position:center center' : $hero['additional_background_css'];
        }


        $content_post = get_post($post->ID);
        $content = $content_post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);

		// If no content or image, bail
		if ( empty( $hero['img'] )) return;

		$min_height = ( empty( $hero['min_height'] ) ? 'min-height: 300px;' : 'min-height: ' . $hero['min_height'] . '; ' );
		$additional_background_css = empty( $hero['additional_background_css'] ) ? '' : ' '.$hero['additional_background_css'];
		?>

      <div class="container-fluid">
        <div class="row d-flex justify-content-center  row-hero" style="<?php echo $min_height; ?>;background-image:<?php echo $gradient; ?> url(<?php echo $hero['img'][0]; ?>);<?php echo $backgroundCSS; ?>">
          <h1 class="petition__heading"><?php the_title(); ?></h1>
          <?php
            echo $content;

           ?>
        </div>
      </div>
		<?php
        wp_reset_postdata();

	}

}
