<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <div class="container">

    <div class="row">
      <div>
        <h1>NY Dems Quick and Dirty Style Guide</h1>
        <h2>Framework</h2>
        <p>We are using Bootstrap 4.0.0. Check docs: <a href="https://getbootstrap.com/docs/4.0">here</a>.</p>
        <p>Please have a look at <a href="https://cssguidelin.es/">Harry Roberts' CSS Style Guide</a> for guidelines we are aiming to follow in front end. It's a 30min to an hour read, but very insightful.</p>
      </div>
    </div>



    <div class="row">
      <div class="col-12">
        <h2>Colors</h2>

        <h3>Grays</h3>

          <div class="row">
            <div class="col-12 col-md-6">
              <div class="p-3 mb-2 bg-light text-dark">.bg-light</div>
            </div>
            <div class="col-12 col-md-6">
              <p>#818a91</p>
            </div>
            <div class="col-12 col-md-6">
              <div class="p-3 mb-2 bg-dark text-white">.bg-dark</div>
            </div>
            <div class="col-12 col-md-6">
              <p>#818a91</p>
            </div>
            <div class="col-12 col-md-6">
              <div class="p-3 mb-2 bg-white text-dark">.bg-white</div>
            </div>
            <div class="col-12 col-md-6">
              <p>#818a91</p>
            </div>
          </div>

        <h3>Content Colors</h3>

          <div class="row">
            <div class="col-12 col-md-6">
              <div class="p-3 mb-2 bg-primary text-white">.bg-primary</div>
            </div>
            <div class="col-12 col-md-6">
              <p>#818a91</p>
            </div>
            <div class="col-12 col-md-6">
              <div class="p-3 mb-2 bg-secondary text-white">.bg-secondary</div>
            </div>
            <div class="col-12 col-md-6">
              <p>#818a91</p>
            </div>
            <div class="col-12 col-md-6">
              <div class="p-3 mb-2 bg-primary-cta text-white">.bg-primary-cta</div>
            </div>
            <div class="col-12 col-md-6">
              <p>#818a91</p>
            </div>
            <div class="col-12 col-md-6">
              <div class="p-3 mb-2 bg-secondary-cta text-white">.bg-secondary-cta</div>
            </div>
            <div class="col-12 col-md-6">
              <p>#818a91</p>
            </div>
          </div>

        <h3>Utility Colors</h3>

          <div class="row">
            <div class="col-12 col-md-6">
              <div class="p-3 mb-2 bg-success text-white">.bg-success</div>
            </div>
            <div class="col-12 col-md-6">
              <p>#818a91</p>
            </div>
            <div class="col-12 col-md-6">
              <div class="p-3 mb-2 bg-danger text-white">.bg-danger</div>
            </div>
            <div class="col-12 col-md-6">
              <p>#818a91</p>
            </div>
            <div class="col-12 col-md-6">
              <div class="p-3 mb-2 bg-warning text-dark">.bg-warning</div>
            </div>
            <div class="col-12 col-md-6">
              <p>#818a91</p>
            </div>
            <div class="col-12 col-md-6">
              <div class="p-3 mb-2 bg-info text-white">.bg-info</div>
            </div>
            <div class="col-12 col-md-6">
              <p>#818a91</p>
            </div>
          </div>

      </div>
    </div>
    <hr class="default style-page">
    <div class="row">
      <div>
        <h2>Forms</h2>
        <h3>
          Hero Petition Form
        </h3>

      <div class="hero-petition__form-demo-wrapper">
      <div class="hero-petition__form-demo">
        <fieldset class="hero-petition__fieldset hero-petition__wrapper">

          <!-- <div class="hero-petition__input-group"> -->
            <input  class="hero-petition hero-petition__email" id="email" type="email" placeholder="email"/>
            <label for="email">Email</label>
          <!-- </div> -->
          <!-- <div class="input-group hero-petition__input-group"> -->
            <input class="hero-petition hero-petition__zip-code" type="zip" placeholder="zip"/>
            <label>Zip code</label>
            <input type="submit" class="nyd-form-base__input--submit hero-petition hero-petition__submit" value="I Agree" />
          <!-- </div> -->

        </fieldset>
      </div>
      </div>
    </div>
  </div>
    <div class="row">
      <div>
        <h2>Buttons</h2>
        <button type="button" class="btn btn-primary">Primary</button>
        <button type="button" class="btn btn-secondary">Secondary</button>
        <button type="button" class="btn btn-success">Success</button>
        <button type="button" class="btn btn-danger">Danger</button>
        <button type="button" class="btn btn-warning">Warning</button>
        <button type="button" class="btn btn-info">Info</button>
        <button type="button" class="btn btn-light">Light</button>
        <button type="button" class="btn btn-dark">Dark</button>

        <button type="button" class="btn btn-link">Link</button>
      </div>
    </div>

    <div class="row">
      <div>
        <h2>Typography</h2>

        <p>This is a paragraph</p>
        <blockquote>This is a blockquote. –<cite>cite me</cite></blockquote>
        <p>If you want me to be <strong>strong</strong> or <em>emphasized</em>, you may do so.</p>
        <p>Display headings 1 - 3 in uppercase by default.</p>

        <h1>Heading One</h1>
        <h2>Heading Two</h2>
        <h3>Heading Three</h3>
        <h4>Heading Four</h4>
        <h5>Heading Five</h5>
        <h6>Heading Six</h6>
      </div>
    </div>

    <div class="row">
      <div>
        <h2>Modules</h2>

        <h3>New Feed List</h3>
        <div class="row">
            <div class="col-12">
              <ul class="list-group newsfeed">
                <li class="list-group-item"><span>label</span>: item</li>
                <li class="list-group-item"><span>label</span>: item</li>
                <li class="list-group-item"><span>label</span>: item</li>
                <li class="list-group-item"><span>label</span>: item</li>
              </ul>
            </div>
        </div>
      </div>
    </div>

  </div>
<?php endwhile; ?>
