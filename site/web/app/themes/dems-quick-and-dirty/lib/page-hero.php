<?php
use Roots\Sage\Extras;
	/**
	 * Add a hero option to pages
	 */


	/**
	 * Create the metabox default values
	 */
	function nydems_page_hero_metabox_defaults() {
		return array(
			'content' => '',
			'content_markdown' => '',
			'image' => '',
			'overlay' => 'off',
			'min_height' => '',
			'overlay_styling' => '',
			'additional_background_css' => '',
		);
	}


	/**
	 * Create a metabox
	 */
	function nydems_page_hero_box() {

		add_meta_box( 'nydems_page_hero_textarea', 'Page Hero', 'nydems_page_hero_textarea', 'page', 'normal', 'default' );

	}
	add_action( 'add_meta_boxes', 'nydems_page_hero_box' );

	/**
	 * Add fields to the metabox
	 */
	function nydems_page_hero_textarea() {

		global $post;

		// Get hero content
		$saved = get_post_meta( $post->ID, 'nydems_page_hero', true );
		$defaults = nydems_page_hero_metabox_defaults();
		$hero = wp_parse_args( $saved, $defaults );

		?>

			<p>Use this section to display a banner above the primary page content.</p>

			<h3>Text and Calls-to-Action</h3>

			<fieldset>
				<?php wp_editor(
					stripslashes( Extras\get_jetpack_markdown( $hero, 'content' ) ),
					'nydems_page_hero_content',
					array(
						'wpautop' => false,
						'textarea_name' => 'nydems_page_hero_content',
						'textarea_rows' => 8,
					)
				); ?>
				<label class="description" for="hero_content"><?php _e( 'Add text and calls to action.', 'nydems' ); ?></label>
			</fieldset>

			<h3>Image or Video</h3>

			<fieldset>
				<label for="nydems_page_hero_image_upload"><?php printf( __( '[Optional] Select an image or video using the Media Uploader. Alternatively, paste the URL for a video hosted on YouTube, Vimeo, Viddler, Instagram, TED, %sand more%s. Example: %s', 'nydems' ), '<a target="_blank" href="http://www.oembed.com/#section7.1">', '</a>', '<code>http://youtube.com/watch/?v=12345abc</code>' ); ?></label>
				<input type="text" class="large-text" name="nydems_page_hero_image" id="nydems_page_hero_image" value="<?php echo stripslashes( $hero['image'] ); ?>"><br>
				<button type="button" class="button" id="nydems_page_hero_image_upload_btn" data-nydems-page-hero="#nydems_page_hero_image"><?php _e( 'Select an Image or Video', 'nydems' )?></button>
			</fieldset>

			<h3>Background Images</h3>

			<p>To add a background image to your hero banner, set a <em>Featured Image</em>.</p>

			<fieldset>
				<input type="checkbox" id="nydems_page_hero_overlay" name="nydems_page_hero_overlay" value="on" <?php checked( 'on',  $hero['overlay'] ); ?>>
				<label for="nydems_page_hero_overlay"><?php _e( 'Add a semi-transparent overlay to the background image to make the text easier to read', 'nydems' ); ?></label>
			</fieldset>

			<h3>Minimum Height</h3>

			<fieldset>
				<label for="nydems_page_hero_image_upload"><?php printf( __( '[Optional] Make sure the hero never gets too small by providing a minimum height. Example: %s', 'nydems' ), '<code>300px</code>' ); ?></label>
				<input type="text" class="large-text" name="nydems_page_hero_min_height" id="nydems_page_hero_min_height" value="<?php echo stripslashes( $hero['min_height'] ); ?>">
			</fieldset>

			<h3>Overlay Styling</h3>
			Find RGBA codes at: <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Colors/Color_picker_tool">RGB color picker</a>

			<fieldset>
				<label for="nydems_page_hero_overlay_styling"><?php printf( __( '[Optional] Change overlay color and transparency from default. Example: %s', 'nydems' ), '<code>rgba(0, 0, 0, 0.5)</code>' ); ?></label>
				<input type="text" class="large-text" name="nydems_page_hero_overlay_styling" id="nydems_page_hero_overlay_styling" value="<?php echo stripslashes( $hero['overlay_styling'] ); ?>">
			</fieldset>

			<h3>Additional Background CSS Styling (Advanced)</h3>

			<fieldset>
				<input type="text" placeholder="background-position: bottom center; background-size: 1500px;" class="large-text" name="nydems_page_hero_additional_background_css" id="nydems_page_hero_additional_background_css" value="<?php echo stripslashes( $hero['additional_background_css'] ); ?>">
				<label for="nydems_page_hero_additional_background_css"><?php _e( 'background-color, background-image, background-repeat, background-attachment, background-position', 'nydems' ); ?></label>
			</fieldset>

		<?php

		// Security field
		wp_nonce_field( 'nydems-page-hero-nonce', 'nydems-page-hero-process' );

	}



	/**
	 * Save field data
	 * @param  number $post_id The post ID
	 * @param  Array $post     The post data
	 */
	function nydems_save_page_hero_textarea( $post_id, $post ) {

		// Verify data came from edit screen
		if ( !isset( $_POST['nydems-page-hero-process'] ) || !wp_verify_nonce( $_POST['nydems-page-hero-process'], 'nydems-page-hero-nonce' ) ) {
			return $post->ID;
		}

		// Verify user has permission to edit post
		if ( !current_user_can( 'edit_post', $post->ID )) {
			return $post->ID;
		}

		// Get hero data
		$hero = array();

		if ( isset( $_POST['nydems_page_hero_content'] ) ) {
			$hero['content'] = Extras\process_jetpack_markdown( wp_filter_post_kses( $_POST['nydems_page_hero_content'] ) );
			$hero['content_markdown'] = wp_filter_post_kses( $_POST['nydems_page_hero_content'] );
		}

		if ( isset( $_POST['nydems_page_hero_image'] ) ) {
			$hero['image'] = wp_filter_post_kses( $_POST['nydems_page_hero_image'] );
		}

		if ( isset( $_POST['nydems_page_hero_color'] ) ) {
			$hero['color'] = wp_filter_nohtml_kses( $_POST['nydems_page_hero_color'] );
		}

		if ( isset( $_POST['nydems_page_hero_overlay'] ) ) {
			$hero['overlay'] = wp_filter_nohtml_kses( $_POST['nydems_page_hero_overlay'] );
		}

		if ( isset( $_POST['nydems_page_hero_overlay_styling'] ) ) {
			$hero['overlay_styling'] = wp_filter_nohtml_kses( $_POST['nydems_page_hero_overlay_styling'] );
		}

		if ( isset( $_POST['nydems_page_hero_min_height'] ) ) {
			$hero['min_height'] = wp_filter_nohtml_kses( $_POST['nydems_page_hero_min_height'] );
		}

		if ( isset( $_POST['nydems_page_hero_additional_background_css'] ) ) {
			$hero['additional_background_css'] = wp_filter_nohtml_kses( $_POST['nydems_page_hero_additional_background_css'] );
		}

		// Update hero settings
		update_post_meta( $post->ID, 'nydems_page_hero', $hero );

	}
	add_action('save_post', 'nydems_save_page_hero_textarea', 1, 2);



	// Save the data with revisions
	function nydems_save_revisions_page_hero_textarea( $post_id ) {

		// Check if it's a revision
		$parent_id = wp_is_post_revision( $post_id );

		// If is revision
		if ( $parent_id ) {

			// Get the data
			$parent = get_post( $parent_id );
			$hero = get_post_meta( $parent->ID, 'nydems_page_hero', true );

			// If data exists, add to revision
			if ( !empty( $hero ) && is_array( $hero ) ) {
				if ( array_key_exists( 'content', $hero ) ) {
					add_metadata( 'post', $post_id, 'nydems_page_hero_content', $hero['content'] );
				}

				if ( array_key_exists( 'content_markdown', $hero ) ) {
					add_metadata( 'post', $post_id, 'nydems_page_hero_content_markdown', $hero['content'] );
				}

				if ( array_key_exists( 'image', $hero ) ) {
					add_metadata( 'post', $post_id, 'nydems_page_hero_image', $hero['image'] );
				}

				if ( array_key_exists( 'color', $hero ) ) {
					add_metadata( 'post', $post_id, 'nydems_page_hero_color', $hero['color'] );
				}

				if ( array_key_exists( 'overlay', $hero ) ) {
					add_metadata( 'post', $post_id, 'nydems_page_hero_overlay', $hero['overlay'] );
				}

				if ( array_key_exists( 'overlay_styling', $hero ) ) {
					add_metadata( 'post', $post_id, 'nydems_page_hero_overlay_styling', $hero['overlay_styling'] );
				}

				if ( array_key_exists( 'min_height', $hero ) ) {
					add_metadata( 'post', $post_id, 'nydems_page_hero_min_height', $hero['min_height'] );
				}

				if ( array_key_exists( 'additional_background_css', $hero ) ) {
					add_metadata( 'post', $post_id, 'nydems_page_hero_additional_background_css', $hero['additional_background_css'] );
				}
			}

		}

	}
	add_action( 'save_post', 'nydems_save_revisions_page_hero_textarea' );



	// Restore the data with revisions
	function nydems_restore_revisions_page_hero_textarea( $post_id, $revision_id ) {

		// Variables
		$post = get_post( $post_id );
		$revision = get_post( $revision_id );
		$hero = get_post_meta( $post_id, 'nydems_page_hero', true );
		$hero_content = get_metadata( 'post', $revision->ID, 'nydems_page_hero_content', true );
		$hero_content_markdown = get_metadata( 'post', $revision->ID, 'nydems_page_hero_content_markdown', true );
		$hero_image = get_metadata( 'post', $revision->ID, 'nydems_page_hero_image', true );
		$hero_color = get_metadata( 'post', $revision->ID, 'nydems_page_hero_color', true );
		$hero_overlay = get_metadata( 'post', $revision->ID, 'nydems_page_hero_overlay', true );
		$hero_min_height = get_metadata( 'post', $revision->ID, 'nydems_page_hero_min_height', true );
		$hero_overlay_styling = get_metadata( 'post', $revision->ID, 'nydems_page_hero_overlay_styling', true );
		$hero_additional_background_css = get_metadata( 'post', $revision->ID, 'nydems_page_hero_additional_background_css', true );

		// Update content
		if ( !empty( $hero_content ) ) {
			$hero['content'] = $hero_content;
		}
		if ( !empty( $hero_content_markdown ) ) {
			$hero['content_markdown'] = $hero_content_markdown;
		}
		if ( !empty( $hero_image ) ) {
			$hero['image'] = $hero_image;
		}
		if ( !empty( $hero_color ) ) {
			$hero['color'] = $hero_color;
		}
		if ( !empty( $hero_overlay ) ) {
			$hero['overlay'] = $hero_overlay;
		}
		if ( !empty( $hero_overlay ) ) {
			$hero['overlay_styling'] = $hero_overlay_styling;
		}
		if ( !empty( $hero_min_height ) ) {
			$hero['min_height'] = $hero_min_height;
		}
		if ( !empty( $hero_additional_background_css ) ) {
			$hero['additional_background_css'] = $hero_additional_background_css;
		}

		update_post_meta( $post_id, 'nydems_page_hero', $hero );

	}
	add_action( 'wp_restore_post_revision', 'nydems_restore_revisions_page_hero_textarea', 10, 2 );



	// Get the data to display the revisions page
	function nydems_get_revisions_field_page_hero_textarea( $fields ) {
		$fields['nydems_page_hero_content'] = 'Page Hero Content';
		$fields['nydems_page_hero_content_markdown'] = 'Page Hero Markdown Content';
		$fields['nydems_page_hero_image'] = 'Page Hero Image or Video';
		$fields['nydems_page_hero_color'] = 'Page Hero Background and Text Color';
		$fields['nydems_page_hero_overlay'] = 'Page Hero Background Overlay';
		$fields['nydems_page_hero_min_height'] = 'Page Hero Minimum Height';
		$fields['nydems_page_hero_overlay_styling'] = 'Page Hero Overlay Styling';
		$fields['nydems_page_hero_additional_background_css'] = 'Page Hero Additional Background CSS';
		return $fields;
	}
	add_filter( '_wp_post_revision_fields', 'nydems_get_revisions_field_page_hero_textarea' );



	// Display the data on the revisions page
	function nydems_display_revisions_field_page_hero_textarea( $value, $field ) {
		global $revision;
		return get_metadata( 'post', $revision->ID, $field, true );
	}
	add_filter( '_wp_post_revision_field_my_meta', 'nydems_display_revisions_field_page_hero_textarea', 10, 2 );



		// Load required scripts and styles
	function nydems_add_page_hero_scripts( $hook ) {

		global $typenow;
		if ( in_array( $typenow, array( 'page', 'post' ) ) ) {
			wp_enqueue_media();

			// Registers and enqueues the required javascript.
			wp_register_script( 'meta-box-image', get_template_directory_uri() . '/lib/page-hero/page-hero.js', array( 'jquery' ) );
			wp_localize_script( 'meta-box-image', 'meta_image',
				array(
					'title' => __( 'Choose or Upload an Image', 'nydems' ),
					'button' => __( 'Use this image', 'nydems' ),
				)
			);
			wp_enqueue_script( 'meta-box-image' );
		}

	}
	add_action( 'admin_enqueue_scripts', 'nydems_add_page_hero_scripts', 10, 1 );




?>
