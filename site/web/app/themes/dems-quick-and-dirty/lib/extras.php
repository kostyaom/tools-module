<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'dems-quick-and-dirty') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

  /**
   * Get saved markdown content if it exists and Jetpack is active. Otherwise, get HTML.
   * @param  array  $options  Array with HTML and markdown content
   * @param  string $name     The name of the content
   * @param  string $suffix   The suffix to denote the markdown version of the content
   * @return string           The content
   */
  function get_jetpack_markdown( $options, $name, $suffix = '_markdown' ) {

    // If markdown class is defined, get markdown content
    if ( class_exists( 'WPCom_Markdown' ) && array_key_exists( $name . $suffix, $options ) && !empty( $options[$name . $suffix] ) ) {
      return $options[$name . $suffix];
    }

    // Else, return HTML
    return $options[$name];

  }

    /**
   * Convert markdown to HTML using Jetpack
   * @param  string $content Markdown content
   * @return string          Converted content
   */
  function process_jetpack_markdown( $content ) {

    // If markdown class is defined, convert content
    if ( class_exists( 'WPCom_Markdown' ) ) {

      // Get markdown library
      jetpack_require_lib( 'markdown' );

      // Return converted content
      return WPCom_Markdown::get_instance()->transform( $content );

    }

    // Else, return content
    return $content;

  }

  // Start BNS Dynamic Copyright
if ( ! function_exists( 'bns_dynamic_copyright' ) ) {
  function bns_dynamic_copyright( $args = '' ) {
      $initialize_values = array( 'start' => '', 'copy_years' => '', 'url' => '', 'end' => '' );
      $args = wp_parse_args( $args, $initialize_values );

      /* Initialize the output variable to empty */
      $output = '';

      /* Start common copyright notice */
      empty( $args['start'] ) ? $output .= sprintf( __('Copyright') ) : $output .= $args['start'];

      /* Calculate Copyright Years; and, prefix with Copyright Symbol */
      if ( empty( $args['copy_years'] ) ) {
        /* Get all posts */
        $all_posts = get_posts( 'post_status=publish&order=ASC' );
        /* Get first post */
        $first_post = $all_posts[0];
        /* Get date of first post */
        $first_date = $first_post->post_date_gmt;

        /* First post year versus current year */
        $first_year = substr( $first_date, 0, 4 );
        if ( $first_year == '' ) {
          $first_year = date( 'Y' );
        }

      /* Add to output string */
        if ( $first_year == date( 'Y' ) ) {
        /* Only use current year if no posts in previous years */
          $output .= ' &copy; ' . date( 'Y' );
        } else {
          $output .= ' &copy; ' . $first_year . "-" . date( 'Y' );
        }
      } else {
        $output .= ' &copy; ' . $args['copy_years'];
      }

      /* Create URL to link back to home of website */
      empty( $args['url'] ) ? $output .= ' <a href="' . home_url( '/' ) . '" title="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" rel="home">' . get_bloginfo( 'name', 'display' ) .'</a>  ' : $output .= ' ' . $args['url'];

      /* End common copyright notice */
      empty( $args['end'] ) ? $output .= ' ' . sprintf( __('All rights reserved.') ) : $output .= ' ' . $args['end'];

      /* Construct and sprintf the copyright notice */
      $output = sprintf( __('<span class="copyright" id="bns-dynamic-copyright"> %1$s </span><!-- #bns-dynamic-copyright -->'), $output );
      $output = apply_filters( 'bns_dynamic_copyright', $output, $args );

      echo $output;
  }
}
// End BNS Dynamic Copyright

/*
 * Configure Custom Logo Support
 * source: https://developer.wordpress.org/themes/functionality/custom-logo/
 */
function dems_quick_and_dirty_custom_logo_setup() {
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', __NAMESPACE__ . '\\dems_quick_and_dirty_custom_logo_setup' );

/*
 * We want our news excerpts to blend well with the Twitter feeds
 * source: https://isabelcastillo.com/different-excerpt-length-for-different-post-types
 */

 function custom_excerpt_length($length) {
    global $post;
    if ($post->post_type == 'news')
      return 32;
    else
      return 80;
}
add_filter('excerpt_length', __NAMESPACE__ . '\\custom_excerpt_length');

/*
* Disable all comments.
*
* source: https://gist.github.com/mattclements/eab5ef656b2f946c4bfb
*/
// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', __NAMESPACE__ . '\\df_disable_comments_post_types_support');

// Close comments on the front-end
function df_disable_comments_status() {
	return false;
}
add_filter('comments_open', __NAMESPACE__ . '\\df_disable_comments_status', 20, 2);
add_filter('pings_open', __NAMESPACE__ . '\\df_disable_comments_status', 20, 2);

// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', __NAMESPACE__ . '\\df_disable_comments_hide_existing_comments', 10, 2);

// Remove comments page in menu
function df_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', __NAMESPACE__ . '\\df_disable_comments_admin_menu');

// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', __NAMESPACE__ . '\\df_disable_comments_admin_menu_redirect');

// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', __NAMESPACE__ . '\\df_disable_comments_dashboard');

// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', __NAMESPACE__ . '\\df_disable_comments_admin_bar');
