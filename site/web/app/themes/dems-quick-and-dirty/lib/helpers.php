<?php

if ( ! function_exists( 'mz_pr' ) ) {
	/**
	 * Print an object to the browser window.
	 * @param  String/Array  $message		What we want to examine in browser
	 */
	function mz_pr($message) {
		echo "<pre>";
		print_r($message);
		echo "</pre>";
	}
}

?>
