<?php
use Roots\Sage\HeroRender;

?>
<header class="navbar navbar-expand-md content navbar-light bg-lignt">


    <div class="col align-self-start position-relative text-nowrap">
      <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
      <svg class="brand__logo"><use xlink:href="#nydems-logo"></use></svg>
      <img class="fallback brand__logo-fallback" src="<?= get_template_directory_uri(); ?>/dist/images/icomoon_pngs/ny-dems-logo.gif" />
      <span class="sr-only"><?php _e('Home', 'dems-quick-and-dirty') ?></span></a>
      <h6 class="navbar__bottom-title position-absolute "><?php echo get_bloginfo('name'); ?></h6>
    </div>

    <div class="col-4 col-md-8 d-flex flex-column flex-nowrap justify-content-between">

      <div class="row d-flex justify-content-end">
      <?php if ( is_active_sidebar( 'header-tools' ) ) : ?>
        <div id="header-tools">
          <?php dynamic_sidebar( 'header-tools' ); ?>
        </div>
      <?php endif; ?>
      </div>

      <div class="row order-md-1 navbar__cta-button-wrapper">
        <button class="btn btn-cta-primary"><?php _e('Donate', 'dems-quick-and-dirty') ?></button>
      </div>

      <div class="row">
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarTogglerHead01" aria-controls="navbarTogglerHead01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <nav class="collapse navbar-collapse nav-primary" id="navbarTogglerHead01" role="navgation">
            <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
            endif;
            ?>
        </nav>
      </div>

    </div>


</header>

<?php HeroRender\nydems_display_hero(); ?>
