<section class="tools-module col-12">
	<h1 class="tools-module__title heading__font-italic">
		<!-- NEW YORK DEMOCRATS ARE FIGHTING BACK. GET INVOLVED -->
		<?php _e('New York Democrats are fighting back. Get involved.', 'dems-quick-and-dirty') ?>
	</h1>
	<ul class="tools-module__list row">
		<li class="tools-module__list-item tools-module__join col-md-6 col-12 col-lg-3">
			<div class="tools-module__info_block">
				<img class="tools-module__join_image" src="<?= get_template_directory_uri(); ?>/dist/images/power.png" alt="#">

				<div class="tools-module__info_title">
					<!-- Join the rapid response team -->
					<?php _e('Join the rapid response team', 'dems-quick-and-dirty') ?>
				</div>
			</div>
		</li>
		<li class="tools-module__list-item tools-module__call-email col-md-6 col-12 col-lg-3">
			<div class="tools-module__info_block">
				
				<img class="tools-module__call-image" src="<?= get_template_directory_uri(); ?>/dist/images/phone.png" alt="#">

				<div class="tools-module__info_title">
					<!-- Make a call or send an email -->
					<?php _e('Make a call or send an email', 'dems-quick-and-dirty') ?>
				</div>
			</div>

		</li>
		<li class="tools-module__list-item tools-module__donate col-md-6 col-12  col-lg-3">
			<div class="tools-module__info_block">
				
				<img class="tools-module__donate_image" src="<?= get_template_directory_uri(); ?>/dist/images/arrow.png" alt="#">

				<div class="tools-module__info_title">
					<!-- Donate to Democrats -->
					<?php _e('Donate to Democrats', 'dems-quick-and-dirty') ?>
				</div>
			</div>
		</li>
		<li class="tools-module__list-item tools-module__donate col-md-6 col-12  col-lg-3">
			<div class="tools-module__info_block">
				
				<img class="tools-module__donate-labtop-image" src="<?= get_template_directory_uri(); ?>/dist/images/laptop.png" alt="#">

				<div class="tools-module__info_title">
					<!-- Signup for webinars & online training -->
					<?php _e('Signup for online training', 'dems-quick-and-dirty') ?>
				</div>
			</div>
		</li>
	</ul>
	<div class="row">
		<div class="ivent_map col-md-6 col-12">
			<div class="ivent_title">FIND OR HOST AN EVENT</div>
			<div class="submit_event">SUBMIT AN EVENT</div>
			<div class="point1 points"><img src="<?= get_template_directory_uri(); ?>/dist/images/point.png" alt="#"></div>
			<div class="point2 points"><img src="<?= get_template_directory_uri(); ?>/dist/images/point.png" alt="#"></div>
			<div class="point3 points"><img src="<?= get_template_directory_uri(); ?>/dist/images/point.png" alt="#"></div>
			<div class="point4 points"><img src="<?= get_template_directory_uri(); ?>/dist/images/point.png" alt="#"></div>
			<div class="point5 points"><img src="<?= get_template_directory_uri(); ?>/dist/images/point.png" alt="#"></div>
			<div class="point6 points"><img src="<?= get_template_directory_uri(); ?>/dist/images/point.png" alt="#"></div>
			<img src="img/ny.jpg" alt="#">
		</div>
		<div class="last_news col-md-6 col-12">
			<div class="last_news_title"><span class="left_hr"></span>THE LATEST<span class="right_hr"></span></div>
			<ul class="list_posts">
				<li class="list-item">PRESS RELEASE: <span class="list_text">text text text text text text text text text</span></li>
				<li class="list-item">PRESS RELEASE: <span class="list_text">text text text text text text text text text</span></li>
				<li class="list-item">PRESS RELEASE: <span class="list_text">text text text text text text text text text</span></li>
				<li class="list-item">PRESS RELEASE: <span class="list_text">text text text text text text text text text</span></li>
				<li class="list-item">PRESS RELEASE: <span class="list_text">text text text text text text text text text</span></li>
			</ul>
		</div>
	</div>
</section>
