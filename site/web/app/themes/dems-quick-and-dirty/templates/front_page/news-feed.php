<?php
use Roots\Sage\Extras;

/*
 * Pull in nost recent two news CPT items and three tweets
 *
 */
?>
<div class="col-12 col-md 6 news-feed">
<h2 class="news-feed__title"> <?php _e('The Latest', 'dems-quick-and-dirty') ?> </h2>
  <ul class="list-group news-feeds__list">
  <?php
  $args = array( 'numberposts' => 2, 'post_type' => 'news');
  $latest_news = get_posts( $args );
  if (isset($latest_news) && !empty($latest_news)):
    foreach($latest_news as $post):
      setup_postdata($post); ?>
      <li class="list-group-item news-feed__list-item news-feed__press-release"><strong><?php _e('Press Release', 'dems-quick-and-dirty') ?></strong>: <?php echo get_the_excerpt(); ?></li>
      <?php wp_reset_postdata();
    endforeach;
  endif;


  // Set configurations
  $configs = [
    // Set here tokens from your Twitter's app
    'consumer_key' => 'bzqwLTGCwo036tJ83Oyv1L3sG',
    'consumer_secret' => 'IrvlN1EWbLQO8W4ZCSieVDRcb9zf5kVQt9hnJOZhK3uBLzJQ05',

    // The Twitter account name
    'screen_name' => 'nydems',

    // The number of tweets
    'count' => 3,
  ];

  if (class_exists('\Netgloo\GetTweetsInPhp')) {
    try {
      $tweets = \Netgloo\GetTweetsInPhp::get_tweets($configs);
      foreach ($tweets as $tweet): ?>

        <li class="list-group-item news-feed__twitter"><strong><?php _e('Twitter Feed', 'dems-quick-and-dirty') ?></strong>: <?php echo $tweet->n_html_text; ?></li>

      <?php
      endforeach;
    } catch (Exception $e) {
      wp_mail('mike@mzoo.org', 'Twitter Feed Error on NY Dems Site', 'Caught exception: '.  $e->getMessage(). "\n");
}
  } else {
    $current_user = wp_get_current_user();
    if (user_can( $current_user, 'administrator' )) { ?>
      <li class="bg-danger text-black p-3">ALERT: Activate <a href="https://wordpress.org/plugins/get-tweets-in-php">get-tweets-in-php plugin</a> to view Tweets.</li>
    <?php } ?>
  <?php } ?>
  </ul>
</div>
