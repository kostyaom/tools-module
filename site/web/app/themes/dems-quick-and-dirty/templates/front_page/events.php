<div class="col-12 col-md 6 events">
  <h2 class="events__title">Find or Host an Event</h2>
  <?php
    if ( ! defined( 'ABSPATH' ) ) {
      die( '-1' );
    } ?>

 <?php

 /* TEST code */
 if (class_exists('Tribe__Events__Template_Factory')) {
    Tribe__Events__Template_Factory::asset_package( 'jquery-placeholder' );
    Tribe__Events__Template_Factory::asset_package( 'bootstrap-datepicker' );
    Tribe__Events__Template_Factory::asset_package( 'tribe-events-bar' );
    do_action( 'tribe-events-bar-enqueue-scripts' );
  }
  /* END TEST code */

  echo do_shortcode('[tribe_mini_calendar]');
  ?>

  <a href="<?php echo get_home_url(); ?>/events/community/add"><button class="btn btn-sm btn-cta-secondary">Submit Event</button></a>

</div>
