<section class="targets container-fluid">
	<div class="section-wrapper targets__section-wrapper">
	<div class="targets__color-overlay">
  	<h1 class="targets__title"><?php _e('The Trump Republicans who have declared war on New York families.', 'dems-quick-and-dirty') ?></h1>
	<ul class="targets__list row">
		<li class="targets__list-item col-12 col-sm-6 col-md-4"><img class="targets__republican-flip-card" src="<?= get_template_directory_uri(); ?>/dist/images/person1.jpg">
			<span class="targets__target-name">Cladia Tenney</span>
			<span class="targets__target-district-name">(R-New Hartford)</span>
			<span class="targets__target-district-number">#NY22</span>
		</li>
		<li class="targets__list-item col-12 col-sm-6 col-md-4"><img class="targets__republican-flip-card" src="<?= get_template_directory_uri(); ?>/dist/images/person2.jpg">
			<span class="targets__target-name">John Faso</span>
			<span class="targets__target-district-name">(R-Kinderhook)</span>
			<span class="targets__target-district-number">#NY19</span>
		</li>
		<li class="targets__list-item col-12 col-sm-6 col-md-4"><img class="targets__republican-flip-card" src="<?= get_template_directory_uri(); ?>/dist/images/person3.jpg">
			<span class="targets__target-name">Lee Zeldin</span>
			<span class="targets__target-district-name">(R-Shirley)</span>
			<span class="targets__target-district-number">#NY1</span>
		</li>
		<li class="targets__list-item col-12 col-sm-6 col-md-4"><img class="targets__republican-flip-card" src="<?= get_template_directory_uri(); ?>/dist/images/person4.jpg">
			<span class="targets__target-name">Tom Reed</span>
			<span class="targets__target-district-name">(R-Corning)</span>
			<span class="targets__target-district-number">#NY23</span>
		</li>
		<li class="targets__list-item col-12 col-sm-6 col-md-4"><img class="targets__republican-flip-card" src="<?= get_template_directory_uri(); ?>/dist/images/person5.jpg">
			<span class="targets__target-name">John Katko</span>
			<span class="targets__target-district-name">(R-Syracuse)</span>
			<span class="targets__target-district-number">#NY24</span>
		</li>
		<li class="targets__list-item col-12 col-sm-6 col-md-4"><img class="targets__republican-flip-card" src="<?= get_template_directory_uri(); ?>/dist/images/person6.jpg">
			<span class="targets__target-name">Chris Collins</span>
			<span class="targets__target-district-name">(R-Clarence)</span>
			<span class="targets__target-district-number">#NY27</span>
		</li>
	</ul>
	</div>
</div>
</section>
