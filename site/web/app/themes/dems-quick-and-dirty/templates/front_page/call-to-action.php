<section class="call-to-action"><!-- row -->
	<div class="call-to-action__contribute-wrapper">
	<div class="call-to-action__contribute  ">
		<div class="call-to-action__contribute-hdr-wrapper contribute-hdr-wrapper-default">
			<h1 class="call-to-action__contribute-title contribute-title-default">Contribute</h1> 			
			
		</div>

		<div class="call-to-action__donor-perfect-btns donor-perfect-btns-default">
			
			<ul class="call-to-action__contribute-btn-list contribute-btn-list-default">
				<li><a href="#" class="call-to-action__contribute-btn-amount">$5</a></li>
				<li><a href="#" class="call-to-action__contribute-btn-amount">$10</a></li>
				<li><a href="#" class="call-to-action__contribute-btn-amount">$25</a></li>
				<li><a href="#" class="call-to-action__contribute-btn-amount">$100</a></li>
				<li><a href="#" class="call-to-action__contribute-btn-amount">Other</a></li>
			</ul>
		</div>
	</div>
	<div class="call-to-action__contribute-full-width">
		<div class="call-to-action__contribute-hdr-wrapper contribute-hdr-wrapper-full-width">
			<h1 class="call-to-action__contribute-title contribute-title-full-width">Contribute</h1> 			
			
		</div>

		<div class="call-to-action__donor-perfect-btns donor-perfect-btns-full-width">
			<ul class="call-to-action__contribute-btn-list contribute-btn-list-full-width">
				<li><a href="#" class="call-to-action__contribute-btn-amount">$5</a></li>
				<li><a href="#" class="call-to-action__contribute-btn-amount">$10</a></li>
				<li><a href="#" class="call-to-action__contribute-btn-amount">$25</a></li>
				<li><a href="#" class="call-to-action__contribute-btn-amount">$100</a></li>
				<li><a href="#" class="call-to-action__contribute-btn-amount">Other</a></li>
			</ul>
		</div>
	</div>
	</div> 
	<div class="call-to-action__action-wrapper row">
		<div class="col-4 call-to-action__register">
			<a href="#" class="cta-hover">
			    <svg class="icon icon-checkbox-checked call-to-action__register-icon"><use xlink:href="#icon-checkbox-checked"></use></svg>
			    <img class="fallback cta--fallback" src="<?= get_template_directory_uri(); ?>/dist/images/icomoon_pngs/checkbox-checked.png" />
				<span class="call-to-action__action-title call-to-action_register-title"><?php _e('Register To Vote', 'dems-quick-and-dirty') ?></span>
			</a>	
		</div>
		<div class="col-4 call-to-action__volunteer">
			<a href="#" class="cta-hover">
	    		<svg class="icon icon-bullhorn call-to-action__volunteer-icon"><use xlink:href="#icon-bullhorn"></use></svg>
	    		<img class="fallback cta--fallback" src="<?= get_template_directory_uri(); ?>/dist/images/icomoon_pngs/bullhorn.png" />
				<span class="call-to-action__action-title call-to-action__volunteer-title"><?php _e('Volunteer', 'dems-quick-and-dirty') ?></span>
			</a>	
		</div>
		<div class="col-4 call-to-action__find-polling">
			<a href="#" class="cta-hover">
	    		<svg class="icon icon-location call-to-action__find-polling-icon"><use xlink:href="#icon-location"></use></svg>
	    		<img class="fallback cta--fallback" src="<?= get_template_directory_uri(); ?>/dist/images/icomoon_pngs/location.png" />
				<span class="call-to-action__action-title call-to-action__find-polling-title"><?php _e('Find Your Polling Place', 'dems-quick-and-dirty') ?></span>
			</a>	
		</div>
	</div>	
</section>
