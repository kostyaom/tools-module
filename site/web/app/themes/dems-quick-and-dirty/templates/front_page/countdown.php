<section class="countdown container-fluid">
<div class="section-wrapper countdown__section-wrapper">
	<h1 class="countdown__title heading__font-thin"><?php _e('The Clock is Ticking!', 'dems-quick-and-dirty'); ?></h1>
		<h2 class="heading__font-thin"><?php _e('November 06, 2018', 'dems-quick-and-dirty'); ?></h2>
	<div id="clock" class="countdown__clock">
  		<div class="countdown__display-item countdown__days">
    		<span class="days"></span>
    		<div class="countdown__smalltext"><?php _e('Days', 'dems-quick-and-dirty'); ?></div>
  		</div>
  		<div class="countdown__display-item countdown__hours">
    		<span class="hours"></span>
    		<div class="countdown__smalltext"><?php _e('Hours', 'dems-quick-and-dirty'); ?></div>
  		</div>
  		<div class="countdown__display-item countdown__minutes">
    		<span class="minutes"></span>
    		<div class="countdown__smalltext"><?php _e('Minutes', 'dems-quick-and-dirty'); ?></div>
  		</div>
	  	<div class="countdown__display-item countdown__seconds">
		    <span class="seconds"></span>
		    <div class="countdown__smalltext"><?php _e('Seconds', 'dems-quick-and-dirty'); ?></div>
	  	</div>
	</div>
  <h5 class="heading__font-thin"><?php _e('On Election Day, hold Republicans accountable for AHCA by casting your vote.', 'dems-quick-and-dirty'); ?></h5>
</div>
</section>
