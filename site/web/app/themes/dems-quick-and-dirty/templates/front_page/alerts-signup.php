<section class="alerts-sign-up container-fluid">
	<div class="section-wrapper alerts-sign-up__section-wrapper">
	<div class="alerts-sign-up__form-wrapper row">
		<div class="alerts-sign-up__col1 col-12 col-md-6 col-lg-6">
			<h1 class="alerts-sign-up__title">Sign Up For Alerts</h1>
			<div class="alerts-sign-up__form">
				<ul class="alerts-sign-up__form-list">
					<li><input class="nyd-form-base__input" type="text" placeholder="Name (Required)"></li>
					<li><input class="nyd-form-base__input" type="email" placeholder="Email (Required)"></li>
					<li class="alerts-sign-up__list-item--zip"><input class="nyd-form-base__input" type="text" placeholder="Zip Code"></li>
					<li class="alerts-sign-up__list-item--mobile"><input class="nyd-form-base__input" type="tel" placeholder="Mobile (Required)"></li>
					<li ><input class="nyd-form-base__input--submit nyd-form-base__input" type="submit" value="Sign Me Up"></li>
				</ul>
			</div>
		</div>
		<div class="alerts-sign-up__col2 col-12 col-md-6 col-lg-6>
			<fieldset class="alerts-sign-up__interest">
				<legend class="nyd-form-base__legend">Check All That Interest You</legend>
				<ul class="nyd-form-base__list-interest">
					<li><input type="checkbox" id="host-an-event" class="interest__host-event"><label>Host An Event</label></li>
					<li><input type="checkbox" id="door-to-door" class="interest__door-to-door"><label>Door To Door Canvassing</label></li>
					<li><input type="checkbox" id="door-to-door" class="interest__phone-banking"><label>Phone Banking</label></li>
				</ul>
			</form>
			</fieldset>
		</div>
	</div>

		<img src="<?= get_template_directory_uri(); ?>/dist/images/ny-dems-logo.png" class="alerts-sign-up__dnc-logo" alt="DNC logo" />
	</div>
</section>