<footer>

  <?php
  $custom_logo_id = get_theme_mod( 'custom_logo' );
  $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' ); ?>
  <div class="footer__nav-donate">

    <?php
    if (has_nav_menu('secondary_navigation')) :
      wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav footer-nav__main']);
    endif;

    ?>
    <button class="btn footer__donate-btn btn-cta-primary"><?php _e('Donate', 'dems-quick-and-dirty') ?></button>
  </div>
	
  <nav class="nav footer__courtesy-nav-wrapper">
		<ul class="footer__courtesy-nav">
			<li><a href="#"><?php _e('Privacy Policy', 'dems-quick-and-dirty') ?></a></li>
			<li><a href="#"><?php _e('Terms of Service', 'dems-quick-and-dirty') ?></a></li>
		</ul>
	</nav>
  

	<h5 class="footer__title"><?php _e('Paid for by the', 'dems-quick-and-dirty'); ?> <span itemscope itemtype="http://schema.org/Organization"><?php _e('New York State Democratic Committee', 'dems-quick-and-dirty'); ?></span></h5>

	<address class="footer__address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
		<span class="address__item" itemprop="addressLocality">420 Lexington Avenue</span>
		<span class="address__item" itemprop="addressRegion">New York, NY</span> <span itemprop="postalCode">10170</span>
	</address>
	<h6 class="copyright">&copy; 2018</h6>


</footer>
