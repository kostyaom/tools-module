<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/front_page/petition'); ?>
  <?php get_template_part('templates/front_page/call-to-action'); ?>
  <?php get_template_part('templates/front_page/targets'); ?>
  <?php get_template_part('templates/front_page/tools-module'); ?>


<section class="container-fluid">
    <div class="row">
      <?php get_template_part('templates/front_page/events'); ?>
      <?php get_template_part('templates/front_page/news-feed'); ?>
    </div>
</section>

  <?php get_template_part('templates/front_page/countdown'); ?>
  <?php get_template_part('templates/front_page/alerts-signup'); ?>
  <?php get_template_part('templates/content', 'page'); ?> <!-- This pulls in content from Admin for this page. -->
  <?php get_template_part('templates/page', 'footer'); ?>
<?php endwhile; ?>
