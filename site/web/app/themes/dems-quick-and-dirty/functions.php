<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/required-plugins.php', // TGM Plugin Install Init.
  'lib/wp_bootstrap_navwalker.php', // Cusom Navwalker (well, more like borrowed)
  'lib/page-hero.php', // Admnin code to allow hero section or page. From Chris Ferdinandi
  'lib/page-hero-render.php', // Functions to render the page hero if exists
  'lib/helpers.php' // A couple of helper tools for development.
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'dems-quick-and-dirty'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);
