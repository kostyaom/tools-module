# New York Democratic Committee WP Theme and Plugin(s) for 2018 Midterm Campaign Season #

### DevOps build based on [Roots](http://roots.io) offerings: ###

 * Trellis, which uses Vagrant (Virtualbox wrapper) for local dev and Ansible for deployment.
 * Bedrock which is a python/rails-style wordpress configuration.
 * Sage 8.5.4 which is a wordpress starter theme utilizing npm Bower, Gulp and requiring php 7.

### History ###

* Version 1.0

### Getting Trellis Going ###

Check out the README file under the Trellis directory.

Will need to install:

	* [Virtualbox](https://www.virtualbox.org/wiki/Downloads) >= 4.3.10
	* [Vagrant](https://www.vagrantup.com/downloads.html) >= 2.0.1

I think the configuration files are set up so site will be accessible at nydems.dev.

### Working with the Sage Theme ###

* Check out the README file under the theme directory
* * site/web/app/themes/nydems-quick-and-dirty/README.md

You'll need to have node installed, then install npm, gulp and bower globally, then
run `npm install` and `bower install` from _within_ the theme directory to install 
the dependencies.

### The "Organizing Hub" Plugin ###

 * At this point it just creates a couple of Custom Post Types:
 * * News
 * * Petitions
 
### Other Plugins ###

[Managing plugins with Composer](https://roots.io/bedrock/docs/composer/), at least where possible.

 * polylang
 * the-events-calendar (googlemap API Key:  AIzaSyAPDaBOZA0BIy3To8qXzgoM3nh1WAPvXIY )
 * the-events-calendar-pro (license: 3477dafbe2f4ac3bd912f08bf23b40236364f90a)
 * the-events-calendar-community-events (license: 2310ab373f90bea4ce1949e8405077ec268b286a)
 * get-tweets-in-php
 * custom-post-type-ui (inactive unless needed)

### Reaching the Team ###

* mike@mzoo.org
* tom@lexwebdev.com

### IMPORT DATA ###

There's a wp-cli export file in the Trellis directory.
If you ssh into the guest machine with `vagrant ssh` that file will be in the `vagrant` directory.
You can run `wp db import /vagrant/nydems_dev_development-[date-string].sql` from the `/srv/www/nydems.dev/current` directory to seed your wp install.

### Error Logs ###

 * `vagrant ssh` into guest and find logs in `/srv/www/nydems.dev/logs`